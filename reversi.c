/*
 * Author: Victoria Lacroix
 */
#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE_Y 8
#define BOARD_SIZE_X 8
#define BOARD_SIZE BOARD_SIZE_Y * BOARD_SIZE_X

typedef enum {
  PLAYER_WHITE, PLAYER_BLACK,
} Player;

Player current_player = PLAYER_WHITE;

typedef enum {
  PIECE_NONE, PIECE_WHITE, PIECE_BLACK,
} Piece;

typedef size_t Move;

Piece board[BOARD_SIZE];
bool valids[BOARD_SIZE];

#define PIECE_AT(y, x) board[y * BOARD_SIZE_X + x]
#define VALID_AT(y, x) valids[y * BOARD_SIZE_X + x]

void resetBoard() {
  for (size_t i = 0; i < BOARD_SIZE; ++i) { board[i] = PIECE_NONE; }
  PIECE_AT(3, 3) = PIECE_WHITE;
  PIECE_AT(4, 4) = PIECE_WHITE;
  PIECE_AT(3, 4) = PIECE_BLACK;
  PIECE_AT(4, 3) = PIECE_BLACK;
}

void generateValidMoves(Player p) {
  const Piece first = (p == PLAYER_WHITE) ? PIECE_BLACK : PIECE_WHITE;
  const Piece second = (p == PLAYER_WHITE) ? PIECE_WHITE : PIECE_BLACK;
  for (size_t y = 0; y < BOARD_SIZE_Y; ++y) {
    for (size_t x = 0; x < BOARD_SIZE_X; ++x) {
      VALID_AT(y, x) = false;
      if (PIECE_AT(y, x) != PIECE_NONE) { continue; }
      for (size_t d = 0; d < 4; ++d) {
        int dy, dx;
        switch (d) {
          case 0: dy = 0; dx = -1; break;
          case 1: dy = 0; dx = 1; break;
          case 2: dy = -1; dx = 0; break;
          case 3: dy = 1; dx = 0; break;
        }
        int iy = y + dy, ix = x + dx;
        if (PIECE_AT(iy, ix) != first) { continue; }
        iy += dy, ix += dx;
        for (; iy >= 0 && ix >= 0 && iy < BOARD_SIZE_Y && ix < BOARD_SIZE_X;
            iy += dy, ix += dx) {
          if (PIECE_AT(iy, ix) == second) {
            VALID_AT(y, x) = true;
            break;
          }
          else if (PIECE_AT(iy, ix) == PIECE_NONE) {
            break;
          }
        }
      }
    }
  }
}

void executeMove(Player p, Move m) {
  size_t y = m / BOARD_SIZE_X;
  size_t x = m % BOARD_SIZE_X;
  Piece old_piece = (p == PLAYER_WHITE) ? PIECE_BLACK : PIECE_WHITE;
  Piece new_piece = (p == PLAYER_WHITE) ? PIECE_WHITE : PIECE_BLACK;
  PIECE_AT(y, x) = new_piece;
  for (size_t d = 0; d < 4; ++d) {
    int dy, dx;
    switch (d) {
      case 0: dy = 0; dx = -1; break;
      case 1: dy = 0; dx = 1; break;
      case 2: dy = -1; dx = 0; break;
      case 3: dy = 1; dx = 0; break;
    }
    for (int iy = y + dy, ix = x + dx;
        iy >= 0 && ix >= 0 && iy < BOARD_SIZE_Y && ix < BOARD_SIZE_X;
        iy += dy, ix += dx) {
      if (PIECE_AT(iy, ix) == new_piece) {
        for (; iy != y || ix != x; iy -= dy, ix -= dx) {
          PIECE_AT(iy, ix) = new_piece;
        }
        break;
      }
      else if (PIECE_AT(iy, ix) == PIECE_NONE) {
        break;
      }
    }
  }
}

void printBoard() {
  printf("Current board:\n");
  for (size_t y = 0; y < BOARD_SIZE_Y; ++y) {
    printf("%02ld:", y * BOARD_SIZE_X);
    for (size_t x = 0; x < BOARD_SIZE_X; ++x) {
      char c;
      switch (PIECE_AT(y, x)) {
        case PIECE_NONE: c = '.'; break;
        case PIECE_WHITE: c = 'W'; break;
        case PIECE_BLACK: c = 'B'; break;
      }
      printf("%c", c);
    }
    printf("\n");
  }
  printf("\n");
}

void printValidMoves(Player p) {
  printf("Valid moves:\n");
  for (size_t y = 0; y < BOARD_SIZE_Y; ++y) {
    printf("%2ld:", y * BOARD_SIZE_X);
    for (size_t x = 0; x < BOARD_SIZE_X; ++x) {
      char c = VALID_AT(y, x) ? 'X' : '.';
      printf("%c", c);
    }
    printf("\n");
  }
  printf("\n");
}

int main(int argc, char *argv[]) {
  resetBoard();
  do {
    printBoard();
    generateValidMoves(current_player);

    bool has_moves_left = false;
    for (size_t i = 0; i < BOARD_SIZE; ++i) {
      has_moves_left |= valids[i];
    }
    if (!has_moves_left) {
      break;
    }

    printValidMoves(current_player);
    Move move = -1;
    do {
      switch (current_player) {
        case PLAYER_WHITE: printf("White move? [0-63] "); break;
        case PLAYER_BLACK: printf("Black move? [0-63] "); break;
      }
      switch (scanf("%lu", &move)) {
        case 0: scanf("%*s"); break;
        case EOF: printf("\nEnd of file found. Aborting...\n"); return 1;
      }
    } while (move >= BOARD_SIZE || !valids[move]);

    executeMove(current_player, move);
    current_player = (current_player == PLAYER_WHITE) ? PLAYER_BLACK
      : PLAYER_WHITE;
    printf("\n");
  } while (true);
  switch (current_player) {
    case PLAYER_WHITE: printf("White has no moves. Black wins!\n\n"); break;
    case PLAYER_BLACK: printf("Black has no moves. White wins!\n\n"); break;
  }
  return 0;
}
